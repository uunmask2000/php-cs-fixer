<?php
/*
 * @Author: VSCode
 * @Date: 2022-11-15 10:24:11
 * @LastEditTime: 2022-11-15 13:42:18
 * @FilePath: \admin\config-fixer.php
 * @Description:  指令 [ php php-cs-fixer.phar fix --config=config-fixer.php]
 *
 */

declare(strict_types=1);
$header = '';

$finder = PhpCsFixer\Finder::create()
    ->files()
    ->in(__DIR__ . '/api/app')
    ->in(__DIR__ . '/front/Helper')
    ->in(__DIR__ . '/front/control')
    ->in(__DIR__ . '/front/config')
    // ->in(__DIR__ . '/front/cache')
    // ->in(__DIR__ . '/front/common')
    ->notName('autoload.php');

$config = new PhpCsFixer\Config();
$config->setFinder($finder)
    ->setRiskyAllowed(true)
    ->setRules([
        '@PSR12' => true, // 使用 PSR-12 的程式碼風格標準
        'strict_param' => false, // 開啟嚴格模式，禁止 PHP 自行轉換類型
        'array_syntax' => ['syntax' => 'short'], // 陣列宣告使用
        'no_unused_imports' => false, //删除没用到的use
        'encoding' => true, //PHP代码必须只使用没有BOM的UTF-8
        // -------
        'ordered_class_elements' => [
            'sort_algorithm' => 'alpha'
        ],
        // -------
        'array_indentation' => true,
        'combine_consecutive_unsets' => true,  //当多个unset使用的时候,合并处理
        'class_attributes_separation' => ['elements' => ['method' => 'one',]],
        'multiline_whitespace_before_semicolons' => false,
        'single_quote' => false, //简单字符串应该使用单引号代替双引号
        'binary_operator_spaces' => [
            'operators' => []
        ],
        'braces' => [
            'allow_single_line_anonymous_class_with_empty_body' => true,
            'allow_single_line_closure' => true,
        ],
        'concat_space' => ['spacing' => 'one'],
        'declare_equal_normalize' => ['space' => 'single'],
        'function_typehint_space' => true,
        'single_line_comment_style' => ['comment_types' => ['hash']],
        'include' => true,
        'lowercase_cast' => true,
        'no_extra_blank_lines' => [
            'tokens' => [
                'curly_brace_block',
                'extra',
                'throw',
                'use',
            ]
        ],
        // 'no_multiline_whitespace_around_double_arrow' => true,
        // 'no_spaces_around_offset' => true,
        'no_whitespace_before_comma_in_array' => true,
        'no_whitespace_in_blank_line' => true,
        'object_operator_without_whitespace' => true,
        'single_blank_line_before_namespace' => true,
        'trim_array_spaces' => true,
        // 'unary_operator_spaces' => true,
        'whitespace_after_comma_in_array' => true,
        // 'space_after_semicolon' => true,
        //'single_blank_line_at_eof' => true,
        // 'no_blank_lines_after_class_opening' => true,
        'method_argument_space' => [
            'on_multiline' => 'ensure_fully_multiline',
            'keep_multiple_spaces_after_comma' => true,
            'after_heredoc' => true
        ],
        'return_type_declaration' => ['space_before' => 'one'],
        'single_trait_insert_per_statement' => true,
        'visibility_required' => ['elements' => ['method', 'property','const']]
    ])
    ->setUsingCache(false)
    ->setIndent("\t")
    ->setLineEnding("\r\n");

return $config;
